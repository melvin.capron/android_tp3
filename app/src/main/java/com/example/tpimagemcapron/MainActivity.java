package com.example.tpimagemcapron;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private final int PICK_IMAGE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init load image button
        Button clickButton = (Button) findViewById(R.id.loadImage);
        clickButton.setOnClickListener(buttonCallback());

        //Init mirror image horizontal button
        Button mirrorImageH = (Button) findViewById(R.id.mirrorImageH);
        mirrorImageH.setOnClickListener(mirrorImage("h"));

        //Init mirror image vertical button
        Button mirrorImageV = (Button) findViewById(R.id.mirrorImageV);
        mirrorImageV.setOnClickListener(mirrorImage("v"));
    }

    private View.OnClickListener buttonCallback() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Init the action get picture intent
                Intent imagePicker = new Intent(Intent.ACTION_GET_CONTENT);
                imagePicker.setType("image/*");
                startActivityForResult(Intent.createChooser(imagePicker, "Select a picture please"), PICK_IMAGE);
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri imagePath = data.getData();

            try {
                //Loads the image stream...
                InputStream imageStream = getContentResolver().openInputStream(imagePath);

                //Setting the options
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inMutable = true;

                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream, null, options);

                //Append to element the data given by the activity result
                ((ImageView)findViewById(R.id.loadedImage)).setImageBitmap(selectedImage);
                ((TextView)findViewById(R.id.loadedPathImage)).setText(imagePath.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private View.OnClickListener mirrorImage(final String type){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Matrix matrix = new Matrix();

                //Based on the type, the matrix should scale differently
                switch(type){
                    case "h":
                        matrix.preScale(-1.0f, 1.0f);
                        break;
                    case "v":
                        matrix.preScale(1.0f, -1.0f);
                        break;
                }

                ImageView image = ((ImageView) findViewById(R.id.loadedImage));
                Bitmap bitmapIn = ((BitmapDrawable)image.getDrawable()).getBitmap();
                //Creating the new bitmap image based on the new given matrix
                Bitmap bitmapOut = Bitmap.createBitmap(bitmapIn, 0, 0, bitmapIn.getWidth(), bitmapIn.getHeight(), matrix, true);

                image.setImageBitmap(bitmapOut);
            }
        };
    }
}